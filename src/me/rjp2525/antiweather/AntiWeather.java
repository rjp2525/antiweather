package me.rjp2525.antiweather;
import java.util.HashMap;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class AntiWeather extends JavaPlugin
  implements Listener
{
  private FileConfiguration config;
  private HashMap<String, Boolean> rainWorlds = new HashMap<String, Boolean>();

  public void onEnable()
  {
    getServer().getPluginManager().registerEvents(this, this);
    this.config = getConfig();
    List<?> worlds = getServer().getWorlds();
    int worldCount = worlds.size();
    for (int i = 0; i < worldCount; i++) {
      World thisWorld = (World)worlds.get(i);
      if (thisWorld.getEnvironment().equals(World.Environment.NORMAL)) {
        boolean setting = this.config.getBoolean(thisWorld.getName(), true);
        this.rainWorlds.put(thisWorld.getName(), Boolean.valueOf(setting));
        this.config.set(thisWorld.getName(), Boolean.valueOf(setting));
      }
    }
    saveConfig();
  }

  public void onDisable() {
  }

  @EventHandler(priority=EventPriority.NORMAL)
  public void onRainStart(WeatherChangeEvent event) {
    if (!event.isCancelled()) {
      ((Boolean)this.rainWorlds.get(event.getWorld().getName())).booleanValue();
      if ((event.toWeatherState()) && (((Boolean)this.rainWorlds.get(event.getWorld().getName())).booleanValue()))
        event.setCancelled(true);
    }
  }

  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (commandLabel.equalsIgnoreCase("aw")) {
      if (!sender.hasPermission("aw.toggle")) {
        sender.sendMessage(ChatColor.RED + "[AW] You don't have permission to toggle the weather!");
      }
      else if (args.length == 0) {
        if (!(sender instanceof Player)) {
          sender.sendMessage("[AW] You must specify a world when executing this command.");
          sender.sendMessage("[AW] aw [world] {on|off}");
          return false;
        }
        Player player = (Player)sender;
        String world = player.getWorld().getName();
        boolean setting = ((Boolean)this.rainWorlds.get(world)).booleanValue();
        if (setting)
          player.sendMessage(ChatColor.GOLD + "[AW] Rain is currently" + ChatColor.RED + "DISABLED" + ChatColor.GOLD + " in the specified world.");
        else
          player.sendMessage(ChatColor.GOLD + "[AW] Rain is currently" + ChatColor.GREEN + "ENABLED" + ChatColor.GOLD + " in the specified world.");
      }
      else if (args.length == 1) {
        String world = args[0];
        if (getServer().getWorld(world) == null) {
          sender.sendMessage(ChatColor.RED + "[AW] World " + ChatColor.WHITE + world + ChatColor.RED + " could not be found.");
          return false;
        }
        if (!getServer().getWorld(world).getEnvironment().equals(World.Environment.NORMAL)) {
          sender.sendMessage(ChatColor.RED + "[AW] You can only check weather status in normal worlds.");
          return false;
        }
        boolean setting = ((Boolean)this.rainWorlds.get(world)).booleanValue();
        if (setting)
          sender.sendMessage(ChatColor.GOLD + "[AW] Rain is " + ChatColor.RED + "DISABLED" + ChatColor.GOLD + " in the specified world.");
        else
          sender.sendMessage(ChatColor.GOLD + "[AW] Rain is " + ChatColor.GREEN + "ENABLED" + ChatColor.GOLD + " in the specified world.");
      }
      else if (args.length == 2) {
        String world = args[0];
        if (getServer().getWorld(world) == null) {
          sender.sendMessage(ChatColor.RED + "[AW] World " + ChatColor.WHITE + world + ChatColor.RED + " could not be found.");
          return false;
        }
        if (!getServer().getWorld(world).getEnvironment().equals(World.Environment.NORMAL)) {
          sender.sendMessage(ChatColor.RED + "[AW] You can only set rain status in normal worlds.");
          return false;
        }
        if ((!args[1].equalsIgnoreCase("on")) && (!args[1].equalsIgnoreCase("off"))) {
          sender.sendMessage(ChatColor.RED + "[AW] Setting not recognized:");
          sender.sendMessage(ChatColor.RED + "[AW] /aw [world] {on|off}");
          return false;
        }
        if (args[1].equalsIgnoreCase("on")) {
          this.rainWorlds.put(world, Boolean.valueOf(true));
          this.config.set(world, Boolean.valueOf(true));
          saveConfig();
          sender.sendMessage(ChatColor.GOLD + "[AW] Rain is " + ChatColor.RED + "DISABLED" + ChatColor.GOLD + " in the specified world.");
          getServer().getWorld(world).setWeatherDuration(1);
          return true;
        }
        this.rainWorlds.put(world, Boolean.valueOf(false));
        this.config.set(world, Boolean.valueOf(false));
        saveConfig();
        sender.sendMessage(ChatColor.GOLD + "[AW] Rain is " + ChatColor.GREEN + "ENABLED" + ChatColor.GOLD + " in the specified world.");
        return true;
      }

    }

    return false;
  }
}